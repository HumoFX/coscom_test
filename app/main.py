import time
import uuid

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.middleware.trustedhost import TrustedHostMiddleware
from fastapi.requests import Request
from fastapi.staticfiles import StaticFiles
from loguru import logger
from starlette.concurrency import iterate_in_threadpool

from app.api import api_router
from app.core.config import settings
from app.core.db import db

origins = [
    '*'
]


def create_app() -> object:
    app = FastAPI(
        title=settings.base_config.PROJECT_NAME,
        openapi_url=settings.base_config.OPENAPI_URL,
        docs_url=settings.base_config.DOCS_URL,
        redoc_url=settings.base_config.REDOC_URL,
        version=settings.base_config.PROJECT_VERSION,
        description=settings.base_config.PROJECT_DESCRIPTION,
        debug=settings.base_config.DEBUG
    )
    app.include_router(api_router)
    # media url
    app.mount("/media", StaticFiles(directory="media"), name="media")
    # Database
    app.add_event_handler("startup", db.connect)
    app.add_event_handler("shutdown", db.disconnect)
    # Middleware
    app.add_middleware(CORSMiddleware, allow_origins=origins, allow_methods=["*"], allow_headers=["*"])
    app.add_middleware(GZipMiddleware, minimum_size=1000)
    app.add_middleware(TrustedHostMiddleware, allowed_hosts=["*"])

    # logger create file
    logger.add("logs/debug.log", rotation="100 MB", retention="10 days", level="DEBUG")
    logger.add("logs/info.log", rotation="100 MB", retention="10 days", level="INFO")
    logger.add("logs/error.log", rotation="100 MB", retention="10 days", level="ERROR")

    return app


app = create_app()


@app.middleware("http")
async def logger_middleware(request: Request, call_next):
    if request.url.path.startswith("/media"):
        return await call_next(request)
    if request.url.path.startswith("/docs"):
        return await call_next(request)
    if request.url.path.startswith("/redoc"):
        return await call_next(request)
    if request.url.path.startswith("/openapi.json"):
        return await call_next(request)
    request_id = uuid.uuid4()
    logger.info(f"==================== Request ID: {request_id} ====================")
    logger.info(f"Request: {request.method} {request.url}")
    logger.info(f"Request Headers: {request.headers}")
    start_time = time.time()
    response = await call_next(request)
    try:
        response_body = [chunk async for chunk in response.body_iterator]
        response.body_iterator = iterate_in_threadpool(iter(response_body))
        logger.info(f"Response: {response.status_code} {response_body[0].decode()}")
    except Exception as e:
        logger.error(e)
    logger.info(f"Response Time: {(time.time() - start_time) * 1000} ms")
    logger.info(f"===================== Request ID: {request_id} ===================")
    response.headers["Request-ID"] = str(request_id)
    return response

