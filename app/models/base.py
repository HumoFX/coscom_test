import uuid
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field
from sqlalchemy import MetaData, Column, Integer, String, Sequence, Numeric, ForeignKey, Boolean, TIMESTAMP, \
    CheckConstraint, Date, BigInteger, JSON, and_, DateTime, text
import sqlalchemy.orm as orm
from sqlalchemy.orm import declarative_base

meta = MetaData(schema="public")
base = declarative_base(metadata=meta)


class Base(base):
    __abstract__ = True
    __table_args__ = {"extend_existing": True}

    id = Column(BigInteger, primary_key=True, autoincrement=True, nullable=False)
    created_at = Column(DateTime(timezone=True), server_default=text("CURRENT_TIMESTAMP"), nullable=False)
    updated_at = Column(
        DateTime(timezone=True),
        nullable=False,
        onupdate=datetime.utcnow(),
        server_default=text("CURRENT_TIMESTAMP"),
    )
    is_deleted = Column(Boolean, nullable=False, default=False)
