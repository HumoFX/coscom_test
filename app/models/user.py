import uuid
from datetime import datetime
from typing import Optional

from sqlalchemy import String, Boolean, select
from sqlalchemy.orm import Mapped, mapped_column

from app.models.base import Base
from app.utils.auth import AuthHandler

UUID_ID = uuid.UUID


class User(Base):
    __tablename__ = "user"
    __table_args__ = {"schema": "public"}
    username: Mapped[str] = mapped_column(String(length=32), unique=True, index=True, nullable=False)
    hashed_password: Mapped[str] = mapped_column(String(length=1024), nullable=False)
    is_active: Mapped[bool] = mapped_column(Boolean, default=True, nullable=False)

    @classmethod
    async def get_by_username(cls, db, username: str, mock=False) -> Optional["User"]:
        if mock:
            user = {
                "id": uuid.uuid4(),
                "username": "admin",
                "is_active": True,
                "hashed_password": AuthHandler().get_password_hash("12345"),
                "created_at": datetime.now(),
                "updated_at": datetime.now(),
            }
            return User(**user)
        query = select(cls).where(cls.username == username)
        results = await db.execute(query)
        result = results.one_or_none()
        if result:
            return result[0]
        return result

    def verify_password(self, password: str) -> bool:
        return AuthHandler().verify_password(password, self.hashed_password)

    def create_token_pair(self) -> dict[str, str]:
        return AuthHandler().encode_login_token(self.username)
