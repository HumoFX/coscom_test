from enum import Enum
from typing import Optional, List

from async_fastapi_jwt_auth import AuthJWT
from pydantic import BaseModel

from app.core.config import settings


class Settings(BaseModel):
    authjwt_secret_key: str = settings.secret_key
    # Configure application to store and get JWT from cookies
    authjwt_token_location: set = {"cookies"}
    # Only allow JWT cookies to be sent over https
    authjwt_cookie_secure: bool = True
    # Enable csrf double submit protection. default is True
    authjwt_cookie_csrf_protect: bool = False
    # Change to 'lax' in production to make your website more secure from CSRF Attacks, default is None
    authjwt_cookie_samesite: str = 'none'
    authjwt_algorithms: List[str] = ["HS256", "HS384", "HS512"]


@AuthJWT.load_config
def get_config():
    return Settings()
