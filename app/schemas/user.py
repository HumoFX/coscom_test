import uuid
from typing import Optional
from pydantic import validator, field_validator, BaseModel, EmailStr


class UserAuth(BaseModel):
    username: str
    password: str
