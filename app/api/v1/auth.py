import aiofiles
from fastapi import Depends
from fastapi.responses import JSONResponse, Response
from fastapi.routing import APIRouter
from fastapi.logger import logger
from fastapi.requests import Request
from ldap3 import ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES
from sqlalchemy import text

from app.core.db import db
from app.core.config import settings
from app.models.user import User
from app.schemas.user import UserAuth
from app.utils.export_csv import ldap_sim_export_csv, ldap_msisdn_export_csv

router = APIRouter()


@router.get("/healthcheck", status_code=200)
async def healthcheck(async_db_session=Depends(db.get_db)):
    try:
        await async_db_session.execute(text("SELECT 1"))
        logger.info("Database connection OK")
        return JSONResponse(status_code=200, content={"message": "OK"})
    except Exception as e:
        logger.error(e)
        return JSONResponse(status_code=500, content={"message": "Internal Server Error"})


@router.post("/ldap", status_code=200)
async def ldap_auth(request: Request, async_db_session=Depends(db.get_db), ldap_conn=Depends(db.get_ldap)):
    try:
        data = await request.json()
        username = data.get("username")
        password = data.get("password")
        if username is None or password is None:
            return JSONResponse(status_code=400, content={"message": "Missing username or password"})
        ldap_conn.bind()
        ldap_conn.search(search_base=settings.ldap_search_base, search_filter=f"(sAMAccountName={username})",
                         attributes=[ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES])
        if ldap_conn.entries:
            ldap_conn.unbind()
            return Response(status_code=200)
        else:
            ldap_conn.unbind()
            return JSONResponse(status_code=401, content={"message": "Invalid username or password"})
    except Exception as e:
        logger.error(e)
        return JSONResponse(status_code=500, content={"message": "Internal Server Error"})


# authentication route with db
@router.post("/db", status_code=200)
async def db_auth(auth_user: UserAuth, async_db_session=Depends(db.get_db)):
    try:
        user = await User.get_by_username(db=async_db_session, username=auth_user.username, mock=True)
        if user is None:
            return JSONResponse(status_code=401, content={"message": "Invalid username or password"})
        if not user.verify_password(auth_user.password):
            return JSONResponse(status_code=401, content={"message": "Invalid username or password"})
        if not user.is_active:
            return JSONResponse(status_code=401, content={"message": "User is not active"})
        login_token = user.create_token_pair()
        return JSONResponse(status_code=200, content=login_token)
    except Exception as e:
        logger.error(e)
        return JSONResponse(status_code=500, content={"message": "Internal Server Error"})


@router.get("/user/list", status_code=200, description="Get user list from LDAP server")
async def ldap_list(ldap_conn=Depends(db.get_ldap)):
    try:
        ldap_conn.bind()
        ldap_conn.search(search_base=settings.ldap_search_base, search_filter=f"(sAMAccountName=*)",
                         attributes=[ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES])
        if ldap_conn.entries:
            ldap_conn.unbind()
            async with aiofiles.open("ldap_list_msisdn.csv", mode="w") as f:
                await f.write("msisdns\n")
                for entry in ldap_conn.entries:
                    if entry['telephoneNumber'] is not None and len(entry['telephoneNumber']) == 12:
                        await f.write(f"{entry['telephoneNumber']}\n")
            sim_file = await ldap_sim_export_csv(ldap_conn.entries)
            msisdn_file = await ldap_msisdn_export_csv(ldap_conn.entries)
            result = {"sim_file": sim_file, "msisdn_file": msisdn_file,
                      "data": [entry.entry_attributes_as_dict for entry in ldap_conn.entries]}
            return JSONResponse(status_code=200, content=result)
        else:
            ldap_conn.unbind()
            return JSONResponse(status_code=401, content={"message": "Invalid username or password"})
    except Exception as e:
        logger.error(e)
        return JSONResponse(status_code=500, content={"message": "Internal Server Error"})


@router.get("/user/{username}", status_code=200)
async def ldap_user(username: str, ldap_conn=Depends(db.get_ldap)):
    try:
        ldap_conn.bind()
        ldap_conn.search(search_base=settings.ldap_search_base, search_filter=f"(sAMAccountName={username})",
                         attributes=[ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES])
        if ldap_conn.entries:
            ldap_conn.unbind()
            return JSONResponse(status_code=200, content=ldap_conn.entries[0].entry_attributes_as_dict)
        else:
            ldap_conn.unbind()
            return JSONResponse(status_code=401, content={"message": "Invalid username or password"})
    except Exception as e:
        logger.error(e)
        return JSONResponse(status_code=500, content={"message": "Internal Server Error"})
