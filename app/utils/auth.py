from datetime import datetime, timedelta

import jwt
from fastapi.security import HTTPBearer
from passlib.context import CryptContext

from app.core.config import settings


class AuthHandler:
    security = HTTPBearer()
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    secret = settings.secret_key
    max_code_length = 6

    def get_random_code(self):
        import random
        return random.randint(10 ** (self.max_code_length - 1), (10 ** self.max_code_length) - 1)

    def get_password_hash(self, password):
        return self.pwd_context.hash(password)

    def verify_password(self, plain_password, hashed_password):
        return self.pwd_context.verify(plain_password, hashed_password)

    def encode_token(self, username, _type):
        payload = dict(
            iss=username,
            sub=_type
        )
        to_encode = payload.copy()
        if _type == "access_token":
            to_encode.update({"exp": datetime.utcnow() + timedelta(minutes=settings.access_token_expire_minutes)})
        elif _type == "refresh_token":
            to_encode.update({"exp": datetime.utcnow() + timedelta(minutes=settings.refresh_token_expire_minutes)})

        return jwt.encode(to_encode, self.secret, algorithm=settings.algorithm)

    def encode_login_token(self, username):
        access_token = self.encode_token(username, "access_token")
        refresh_token = self.encode_token(username, "refresh_token")

        login_token = dict(
            access_token=f"{access_token}",
            refresh_token=f"{refresh_token}"
        )
        return login_token

    def encode_update_token(self, username):
        access_token = self.encode_token(username, "access_token")

        update_token = dict(
            access_token=f"{access_token}"
        )
        return update_token

    def decode_access_token(self, token):
        try:
            payload = jwt.decode(token, self.secret, algorithms=[settings.algorithm])
            return payload['sub']
        except jwt.ExpiredSignatureError as e:
            raise Exception("Access token expired")
        except jwt.InvalidSignatureError as e:
            raise Exception("Invalid access token")
        except jwt.InvalidTokenError as e:
            raise Exception("Invalid access token")

        except Exception as e:
            raise Exception("Invalid access token")




