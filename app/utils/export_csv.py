from aiofiles import open as aio_open, os as aio_os
from app.core.config import settings


async def ldap_msisdn_export_csv(data):
    path = f"{settings.media_path}/csv"
    if not await aio_os.path.exists(path):
        await aio_os.mkdir(path)
    file_path = f"{path}/ldap_list_msisdn.csv"
    async with aio_open(file_path, "w") as f:
        await f.write("msisdns\n")
        for entry in data:
            if entry['telephoneNumber'] is not None and len(entry['telephoneNumber']) == 12:
                await f.write(f"{entry['telephoneNumber']}\n")
    return file_path


async def ldap_sim_export_csv(data):
    path = f"{settings.media_path}/csv"
    if not await aio_os.path.exists(path):
        await aio_os.mkdir(path)
    file_path = f"{path}/ldap_list_sim.csv"
    async with aio_open(file_path, "w") as f:
        await f.write("sim\n")
        for entry in data:
            if entry['mobile'] is not None and len(entry['mobile']) == 20:
                await f.write(f"{entry['mobile']}\n")
    return file_path
