import os
from typing import Optional

import pytz
from dotenv import dotenv_values
from loguru import logger
from pydantic import PostgresDsn, Field, BaseModel
from pydantic_settings import BaseSettings, SettingsConfigDict


class FastAPIConfig(BaseModel):
    PROJECT_NAME: Optional[str] = "COSCOM TEST TASK"
    PROJECT_DESCRIPTION: Optional[str] = "COSCOM TEST TASK API"
    PROJECT_VERSION: Optional[str] = "0.0.1"
    DEBUG: Optional[bool] = True
    DOCS_URL: Optional[str] = "/docs"
    REDOC_URL: Optional[str] = "/redoc"
    OPENAPI_URL: Optional[str] = "/openapi.json"
    API_V1_STR: Optional[str] = "/api/v1"

    # Timezone
    TIMEZONE: str = "Asia/Tashkent"
    TIMEZONE_TZ: pytz.timezone = pytz.timezone(TIMEZONE)


class JWTConfig(BaseModel):
    # get from env
    JWT_SECRET: str = dotenv_values(".env").get("JWT_SECRET")
    JWT_ALGORITHM: str = dotenv_values(".env").get("JWT_ALGORITHM")
    JWT_EXPIRE_MINUTES: int = dotenv_values(".env").get("JWT_EXPIRE_MINUTES")
    JWT_REFRESH_EXPIRE_MINUTES: int = dotenv_values(".env").get("JWT_REFRESH_EXPIRE_MINUTES")


class LDAPConfig(BaseModel):
    # get from env
    LDAP_HOST: str = dotenv_values(".env").get("LDAP_HOST", "localhost")
    LDAP_PORT: int = int(dotenv_values(".env").get("LDAP_PORT", 389))
    LDAP_USER: str = dotenv_values(".env").get("LDAP_USER")
    LDAP_PASSWORD: str = dotenv_values(".env").get("LDAP_PASSWORD")
    LDAP_BASE: str = dotenv_values(".env").get("LDAP_BASE")
    LDAP_URI: Optional[str] = f"ldap://{LDAP_HOST}:{LDAP_PORT}/{LDAP_BASE}"


class PostgresConfig(BaseModel):
    # get from env
    POSTGRES_HOST: str = dotenv_values(".env").get("POSTGRES_HOST")
    POSTGRES_PORT: int = dotenv_values(".env").get("POSTGRES_PORT")
    POSTGRES_DB: str = dotenv_values(".env").get("POSTGRES_DB")
    POSTGRES_USER: str = dotenv_values(".env").get("POSTGRES_USER")
    POSTGRES_PASSWORD: str = dotenv_values(".env").get("POSTGRES_PASSWORD")
    POSTGRES_URI: Optional[
        str] = f"postgresql+asyncpg://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}"


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_file_encoding='utf-8')
    base_config: FastAPIConfig = FastAPIConfig()
    # jwt config
    jwt_config: JWTConfig = JWTConfig()

    postgres_user: str
    postgres_password: str
    postgres_host: str
    postgres_port: int
    postgres_db: str
    db_echo: bool
    secret_key: str
    algorithm: str
    access_token_expire_minutes: int
    refresh_token_expire_minutes: int
    debug: bool = True
    ldap_host: str
    ldap_port: int
    ldap_user: str
    ldap_password: str
    ldap_base: str
    postgres_conf: PostgresConfig = PostgresConfig()
    ldap_conf: LDAPConfig = LDAPConfig()
    media_path: str = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'media')
    postgres_dsn: PostgresDsn = Field(postgres_conf.POSTGRES_URI, validation_alias='POSTGRES_DSN')


settings = Settings()
logger.info(f"Settings: {settings.ldap_conf}")
