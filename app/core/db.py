import contextlib
from typing import AsyncIterator, AsyncGenerator

from fastapi import Depends
from sqlalchemy.ext.asyncio import (
    AsyncConnection,
    AsyncSession,
    async_sessionmaker,
    create_async_engine,
)
from ldap3 import Server, Connection, ALL, NTLM, ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES, AUTO_BIND_NO_TLS, SUBTREE
from ldap3.core.exceptions import LDAPException, LDAPBindError, LDAPSocketOpenError, LDAPSocketReceiveError, \
    LDAPSocketSendError

from sqlalchemy.orm import declarative_base
from app.core.config import settings
from loguru import logger


class Database:
    def __init__(self):
        self.__session = None
        self.__engine = None
        self.__ldap = None
        self.__ldap_conn = None

    def connect(self):
        self.__engine = create_async_engine(
            url=str(settings.postgres_dsn),
            echo=settings.db_echo,

        )

        self.__session = async_sessionmaker(
            bind=self.__engine,
            autocommit=False,
        )
        try:
            self.__ldap = Server(settings.ldap_conf.LDAP_URI, get_info=ALL)
            self.__ldap_conn = Connection(self.__ldap, user=settings.ldap_conf.LDAP_USER,
                                          password=settings.ldap_conf.LDAP_PASSWORD,
                                          authentication=NTLM, auto_bind=AUTO_BIND_NO_TLS)
        except Exception as e:
            logger.error(f"LDAP connection error: {e}")

    async def disconnect(self):
        await self.__engine.dispose()
        try:
            self.__ldap_conn.unbind()
        except Exception as e:
            logger.error(f"LDAP unbind error: {e}")

    async def get_db(self) -> AsyncGenerator[AsyncSession, None]:
        async with self.__session() as session:
            yield session

    def get_ldap(self):
        return self.__ldap_conn

    def get_ldap_server(self):
        return self.__ldap


db = Database()
